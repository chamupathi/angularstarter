import { Component, OnInit, Input } from '@angular/core';
import { Rainasa } from '../rainasa';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  @Input("headerInfo") headerInfo :Rainasa;

  constructor() { }

  ngOnInit() {
  }


}
