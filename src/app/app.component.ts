import { Component } from '@angular/core';
import { Rainasa } from './rainasa';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'rainasaA3';
  myDetails : Rainasa = {
                sid     : 991653223,
                sname   : "saharsh ",
                scampus : "Trafalgar",
                slogin  : "rainasa",
                atitle  : "Assignment 3"
              };
}
