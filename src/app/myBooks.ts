export var Data = [
    {'bname' : 'Pride and Prejudice', 'bauthor' :'P.Jane Austen','bgenre':'Novel','year':2012},
    {'bname' : 'Harry Potter', 'bauthor':'J. K. Rowling','bgenre':'Fantasy Fiction','year':2000},
    {'bname' : 'The Village by the Sea', 'bauthor' :'Anita Desai Lal Nehru','bgenre':'Non-fiction','year':1982},
    {'bname' : 'The Lord of the Rings', 'bauthor' :'J. R. R. Tolkien','bgenre':'Novel','year':1954}
];