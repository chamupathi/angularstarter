export class BookInfo {
    bname   : string;
    bauthor : string;
    bgenre  : string;
    year    : number;

}